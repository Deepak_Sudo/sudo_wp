<?php
//default page layout
get_header();

	if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
<div class="page">
  <div class="page__title">
  <?php
  		the_title();
  ?>
  </div>
  <div class="page__content">
  <?php
    the_content();
  ?>
  </div>
  <?php

	endwhile; endif;
  ?>
</div>
<?php
get_footer();


?>
