

<!-- .get in touch -->
<section class="get-in-touch">
  <div class="container">
<?php if( have_rows('get_in_touch_section','option') ): ?>
    <?php while( have_rows('get_in_touch_section','option') ): the_row(); 

  
?>


    <h2><?php  echo get_sub_field("get_in_touch_head",'option');  ?></h2>
    <ul class="social-nav">

    <?php if( have_rows('social_media','option') ): ?>
    <?php while( have_rows('social_media','option') ): the_row(); 

        $social_media_type  = get_sub_field("social_media_type",'option');

        $social_media_link = get_sub_field("social_media_link",'option');


      ?>
      <li><a href="<?php echo $social_media_link; ?>"><i class="fa fa-<?php echo $social_media_type; ?>" aria-hidden="true"></i></a></li>
       <?php

  endwhile;

  endif;


    ?>
      
    </ul>

   
   
    <div class="btn-group">

      <?php

       if( have_rows('email_button','option') ): 
     while( have_rows('email_button','option') ): the_row(); 

  
?>


    
      <a href="mailto:<?php echo get_sub_field('email_address');?>" class="btn btn-white"><?php  echo get_sub_field("email_button_name");   ?></a>

      <?php

    endwhile;
  endif;

      ?>


      <?php 
        if( have_rows('call_button','option') ): 
     while( have_rows('call_button','option') ): the_row(); 



      ?>
      <a href="tel:<?php  echo get_sub_field('call_button_number'); ?>" class="btn btn-white"><?php  echo get_sub_field('call_button_text'); ?></a>

      <?php 

    endwhile;

  endif;

      ?>
    </div>
  </div>
</section><!-- /.get in touch -->

<?php

endwhile;

endif;

?>




<!-- .footer -->
<section class="footer d-none d-sm-block">
  <div class="container">
    <a href="#" class="brand-logo">
      <img src="<?php echo get_template_directory_uri().'/assets/img/brand-logo.png' ?>" alt="Sudo">
    </a>
    <div class="row">
      <div class="col-xs-12 col-sm-3 col-md-3">
        <div class="footer-block">
          <h3>Addresses</h3>

  

          <?php

          // check if the repeater field has rows of data
          if( have_rows('office_address','option') ):


          // loop through the rows of data
          while ( have_rows('office_address','option') ) : the_row();
            ?>

            <div class="address-block">
            <h4><?php   the_sub_field('address_name','option');        ?></h4>
            <p> <?php the_sub_field('address_location','option'); ?>   </p>
          </div>
         
          <?php
           

          endwhile;

          else :

          // no rows found

          endif;

          ?>

        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3">
        <div class="footer-block">
          <h3>Our Services</h3>
          <ul class="footer-nav">
           <?php wp_nav_menu( array( 'theme_location' => 'our_services' ) ); ?>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3">
        <div class="footer-block">
          <h3>Our Links</h3>
          <ul class="footer-nav">
             <?php wp_nav_menu( array( 'theme_location' => 'our_links' ) ); ?>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-3 col-md-3">
        <div class="footer-block">

            <?php    the_field('contact_details','option');    ?>
          

        </div>
      </div>
    </div>
  </div>
</section><!-- /.footer -->
<!-- .footer mobile -->
<section class="footer footer-mobile d-block d-sm-none">
  <div class="container header-wrapper">
    <a href="#" class="brand-logo">
      <img src="<?php echo get_template_directory_uri().'/assets/img/brand-logo.png' ?>" alt="Sudo">
    </a>
    <!--Accordion wrapper-->
    <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
      <!-- Accordion card -->
      <div class="card footer-block">
        <!-- Card header -->
        <div class="card-header" role="tab" id="headingOne1">
          <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
            aria-controls="collapseOne1">
            <h3 class="mb-0">Addresses <i class="fa fa-angle-down rotate-icon"></i></h3>
          </a>
        </div>
        <!-- Card body -->
        <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1"
          data-parent="#accordionEx">
          <div class="card-body">

            <?php

          // check if the repeater field has rows of data
          if( have_rows('office_address','option') ):


          // loop through the rows of data
          while ( have_rows('office_address','option') ) : the_row();
            ?>

            <div class="address-block">
            <h4><?php   the_sub_field('address_name','option');        ?></h4>
            <p> <?php the_sub_field('address_location','option'); ?>   </p>
          </div>
         
          <?php
           

          endwhile;

          else :

          // no rows found

          endif;

          ?>

          </div>
        </div>
      </div><!-- Accordion card -->
      <!-- Accordion card -->
      <div class="card footer-block">
        <!-- Card header -->
        <div class="card-header" role="tab" id="headingTwo2">
          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
            aria-expanded="false" aria-controls="collapseTwo2">
            <h3 class="mb-0">Our Services <i class="fa fa-angle-down rotate-icon"></i></h3>
          </a>
        </div>
        <!-- Card body -->
        <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2"
          data-parent="#accordionEx">
          <div class="card-body">
            <ul class="footer-nav bt-1">
            <?php wp_nav_menu( array( 'theme_location' => 'our_services' ) ); ?>
          </ul>
          </div>
        </div>
      </div><!-- Accordion card -->
      <!-- Accordion card -->
      <div class="card footer-block">
        <!-- Card header -->
        <div class="card-header" role="tab" id="headingThree3">
          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
            aria-expanded="false" aria-controls="collapseThree3">
            <h3 class="mb-0">Our Links <i class="fa fa-angle-down rotate-icon"></i></h3>
          </a>
        </div>
        <!-- Card body -->
        <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3"
          data-parent="#accordionEx">
          <div class="card-body">
            <ul class="footer-nav bt-1">
              <?php wp_nav_menu( array( 'theme_location' => 'our_links' ) ); ?>
            </ul>
          </div>
        </div>
      </div><!-- Accordion card -->
      <!-- Accordion card -->
      <div class="card footer-block">
        <!-- Card header -->
        <div class="card-header" role="tab" id="headingThree4">
          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree4"
            aria-expanded="false" aria-controls="collapseThree4">
            <h3 class="mb-0">Contact Us <i class="fa fa-angle-down rotate-icon"></i></h3>
          </a>
        </div>
        <!-- Card body -->
        <div id="collapseThree4" class="collapse" role="tabpanel" aria-labelledby="headingThree4"
          data-parent="#accordionEx">
          <div class="card-body">
            <ul class="footer-nav bt-1">
            <?php    the_field('contact_details','option');    ?>
          </ul>
          </div>
        </div>
      </div><!-- Accordion card -->
    </div><!-- Accordion wrapper -->
  </div>
</section><!-- /.footer mobile -->
<!-- .copyright -->
<section class="copyright d-none d-sm-block">
  <div class="container">
    <span>&copy; <?php echo date("Y"); ?> </span> <span><?php the_field('copyright_description','option'); ?> </span>
  </div>
</section><!-- /.copyright -->
<!-- .copyright mobile -->
<section class="copyright d-block d-sm-none">
  <div class="container header-wrapper">
    <h4>Connect with us</h4>
    <ul class="social-nav">
      <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
    </ul>
    <span>&copy; <?php echo date("Y"); ?> </span> <span><?php the_field('copyright_description','option'); ?> </span>
  </div>
</section><!-- /.copyright mobile -->
<a href="#" id="toTopBtn" class="cd-top text-replace js-cd-top cd-top--is-visible cd-top--fade-out" data-abc="true"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri().'/assets/bootstrap/js/bootstrap.min.js' ?>"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
<script src="<?php echo get_template_directory_uri().'/assets/js/custom.js' ?>"></script>
</body>
</html>