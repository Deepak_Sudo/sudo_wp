<?php 

/**
 * front-page.php
 * Load Front Page
 * @package WordPress
 * @subpackage mihelp
 *
 */


get_header(); 

?>


<!-- .hero -->
<section class="hero banner-full home-hero d-none d-sm-block">
  <div class="container">
    <!-- .hero content -->
    <div class="hero-content">
      <h1><span>C</span>reate <span>C</span>ode <br> <span>C</span>onsult <span>C</span>are</h1>
      <p><?php the_field("Intro _text");   ?></p>
      <a href="#" class="btn btn-red font-size-15 width-160 mt-40">
        <?php

        if( have_rows('introduction_button') ):

        while( have_rows('introduction_button') ): the_row();


        //group name +underscore + sub_field name
        //the_field("introduction_button_introduction_button_text");

        $a = get_sub_field("introduction_button_text");

        echo $a;

        endwhile;
        endif;

        ?>

      </a>
    </div><!-- /.hero content -->
  </div>
  <!-- .video -->
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop" bgcover="<?php echo get_template_directory_uri().'/assets/img/home-hero-cover.jpg'; ?>">
    <source src="<?php echo get_template_directory_uri().'/assets/video/home-hero-video.mp4' ?>" type="video/mp4">
  </video><!-- /.video -->
  <!-- .banner bottom bar -->
  <div class="banner-bottom-bar">
    <div class="container"></div>
  </div><!-- /.banner bottom bar -->
</section><!-- /.hero -->
<!-- .hero -->
<section class="hero banner-full home-hero d-block d-sm-none">
  <div id="HomeHeroMobile" class="carousel slide height-100" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#HomeHeroMobile" data-slide-to="0" class="active"></li>
        <li data-target="#HomeHeroMobile" data-slide-to="1"></li>
        <li data-target="#HomeHeroMobile" data-slide-to="2"></li>
        <li data-target="#HomeHeroMobile" data-slide-to="3"></li>
        <li data-target="#HomeHeroMobile" data-slide-to="4"></li>
      </ol>
      <div class="carousel-inner height-100">
        <div class="carousel-item active">
          <div class="container header-wrapper">
            <div class="hero-content">
              <h1><span>C</span>reate <br> <span>C</span>ode <br> <span>C</span>onsult <br> <span>C</span>are</h1>
              <p><?php the_field("Intro _text");   ?></p>
              <a href="#" class="btn btn-red font-size-15 width-160 mt-40"> 
                <?php

                if( have_rows('introduction_button') ):

                while( have_rows('introduction_button') ): the_row();


                //group name +underscore + sub_field name
                //the_field("introduction_button_introduction_button_text");

                $a = get_sub_field("introduction_button_text");

                echo $a;

                endwhile;
                endif;

              ?>
            
            </a>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="container header-wrapper">
            <div class="hero-content">
              <h1><span>C</span>reate</h1>
              <p><?php the_field("Intro _text");   ?></p>
              <a href="#" class="btn btn-red font-size-15 width-160 mt-40">
                
             
                <?php

                  if( have_rows('introduction_button') ):

                  while( have_rows('introduction_button') ): the_row();


                  //group name +underscore + sub_field name
                  //the_field("introduction_button_introduction_button_text");

                  $a = get_sub_field("introduction_button_text");

                  echo $a;

                  endwhile;
                  endif;

                  ?>
              </a>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="container header-wrapper">
            <div class="hero-content">
              <h1><span>C</span>ode</h1>
              <p><?php the_field("Intro _text");   ?></p>
              <a href="#" class="btn btn-red font-size-15 width-160 mt-40">
                 href="#" class="btn btn-red font-size-15 width-160 mt-40"> 
                <?php

                if( have_rows('introduction_button') ):

                while( have_rows('introduction_button') ): the_row();


                //group name +underscore + sub_field name
                //the_field("introduction_button_introduction_button_text");

                $a = get_sub_field("introduction_button_text");

                echo $a;

                endwhile;
                endif;

                ?>
                  

              </a>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="container header-wrapper">
            <div class="hero-content">
              <h1><span>C</span>osult</h1>
              <p><?php the_field("Intro _text");   ?></p>
              <a href="#" class="btn btn-red font-size-15 width-160 mt-40">
                 href="#" class="btn btn-red font-size-15 width-160 mt-40"> 
                <?php

                if( have_rows('introduction_button') ):

                while( have_rows('introduction_button') ): the_row();


                //group name +underscore + sub_field name
                //the_field("introduction_button_introduction_button_text");

                $a = get_sub_field("introduction_button_text");

                echo $a;

                endwhile;
                endif;

                ?>
              </a>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="container header-wrapper">
            <div class="hero-content">
              <h1><span>C</span>are</h1>
              <p><?php the_field("Intro _text");   ?></p>
              <a href="#" class="btn btn-red font-size-15 width-160 mt-40">
                 href="#" class="btn btn-red font-size-15 width-160 mt-40"> 
                <?php

                if( have_rows('introduction_button') ):

                while( have_rows('introduction_button') ): the_row();


                //group name +underscore + sub_field name
                //the_field("introduction_button_introduction_button_text");

                $a = get_sub_field("introduction_button_text");

                echo $a;

                endwhile;
                endif;

                ?>


              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
</section><!-- /.hero -->
<!-- .expertise -->
<section class="expertise">
  


      <?php


        // Check rows exists.
      if( have_rows('expertise') ):

          $count=1;

          // Loop through rows.
      while( have_rows('expertise') ) : the_row();


        if($count % 2 != 0 ){

          ?>

        <div class="container">
        <div class="row">

          <div class="col-xs-12 col-sm-6 col-md-6">
        <!-- .software development -->
        <article class="article-content text-right">
          <div class="align-content pr-15">
            <h3><?php  echo  get_sub_field("expertise_title");      ?></h3>
            <h6><?php    echo get_sub_field("expertise_sub_title");      ?></h6>
            <p><?php    echo get_sub_field("expertise_description");      ?></p>
            <div class="d-block d-sm-none btn-mobile">
              <a href="#" class="btn btn-red font-size-15"><?php    echo get_sub_field("expertise_button_text");  ?><span class="arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
            </div>
          </div>
        </article><!-- /.software development -->
      </div>
      <div class="col-xs-12 col-sm-6 col-md-6">
        <!-- .software development figure -->

        <div class="article-figure software-dev opacity-7 d-none d-sm-block" style="background-image:url(<?php  echo get_sub_field("expertise_image")["url"];  ?>) ">
          <div class="figure-content">
            <p><?php    echo get_sub_field("expertise_image_description");  ?></p>
            <h2><?php    echo get_sub_field("expertise_image_heading");  ?></h2> 
          </div>
        </div><!-- /.software development figure -->
      </div>

       </div>
  </div>
           
            

           <!-- /.software development mobile -->
  <div class="article-figure software-dev d-block d-sm-none" style="background-image:url(<?php  echo get_sub_field("expertise_image")["url"];  ?>) "></div><!-- /.software development mobile -->



<?php
            }else{ ?>



               <div class="container">
              <div class="spacer-25"></div>
              <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                  <!-- .application development figure -->
                  <div class="article-figure application-dev opacity-7 d-none d-sm-block" style="background-image:url(<?php  echo get_sub_field("expertise_image")["url"];  ?>) "></div><!-- /.application development figure -->
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                  <!-- .application development -->
                  <article class="article-content">
                    <div class="align-content pl-15">
                      <h3><?php  echo  get_sub_field("expertise_title");      ?></h3>
                      <h6><?php    echo get_sub_field("expertise_sub_title");      ?></h6>
                      <p><?php    echo get_sub_field("expertise_description");      ?></p>
                      <div class="d-block d-sm-none btn-mobile">
                        <a href="#" class="btn btn-red font-size-15"><?php    echo get_sub_field("expertise_button_text");  ?> <span class="arrow"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></a>
                      </div>
                    </div>
                  </article><!-- /.application development -->
                </div>
              </div>
            </div>
            <!-- /.software development mobile -->
            <div class="article-figure application-dev d-block d-sm-none" style="background-image:url(<?php  echo get_sub_field("expertise_image")["url"];  ?>) "></div><!-- /.software development mobile -->

        <?php
            }

            $count++;


            ?>    

        <?php


          // End loop.
          endwhile;

          // No value.
          else :
              // Do something...
          endif;

      ?>

</section><!-- .expertise -->










<!-- .features -->
<section class="features d-none d-sm-block">
  <div class="container">
    

        <?php

        if( have_rows('development_features') ):

        while( have_rows('development_features') ): the_row();

          ?>
          <!-- .section heading -->
    <div class="features-content section-head">


        <h3>
          <?php

          //group name +underscore + sub_field name
          the_field("development_features_development_feature_heading");
           ?> 
         </h3>

         <p><?php

          //group name +underscore + sub_field name
         echo  get_sub_field("features_description");
           ?> </p>
    </div><!-- /.section heading -->

          

              <?php


                // Check rows exists.
        if( have_rows('features') ):

          $features =array();

          $features_des =array();

          $i=0;

            // Loop through rows.
            while( have_rows('features') ) : the_row(); 



        $features[$i] = get_sub_field("feature_name");

        $features_des[$i] = get_sub_field("feature_detail");
        $i++;


            // End loop.
            endwhile;

        // No value.
        else :
            // Do something...
        endif;


            ?>

      

        <?php

        endwhile;
        endif;

        ?>

<?php 

//slice the features arrays for displaying in left and right section


$feature_length= count($features);

if(($feature_length%2) ==0){


$mid_feature = ($feature_length/2);

}else{



$mid_feature = (($feature_length/2)+1);


}



$feature_left = array_slice($features, 0,$mid_feature);

$feature_right = array_slice($features, $mid_feature,$feature_length-1);


$features_des_left = array_slice($features_des,0,$mid_feature);

$features_des_right = array_slice($features_des,$mid_feature,$feature_length-1);


?>


<div class="row">

      <div class="col-xs-12 col-sm-4 col-md-4">
        <ul class="features-list fl-right text-right">

<?php

$feature_left_length = count($feature_left);


$features_des_left_length = count($features_des_left);




$i=0;

while($i<=($feature_left_length -1)){ ?>

          <li>
            <h5><?php   echo $feature_left[$i];     ?></h5>
            <p><?php   echo $features_des_left[$i];     ?></p>
          </li>


<?php

$i++;


}


?> 

</ul>

</div>


 <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="mobile-skeleton"></div>
</div>



<div class="col-xs-12 col-sm-4 col-md-4">
        <ul class="features-list fl-right text-right">


<?php

$feature_right_length = count($feature_right);





$i=0;

while($i<=($feature_right_length -1)){ ?>

          <li>
            <h5><?php   echo $feature_right[$i];     ?></h5>
            <p><?php   echo $features_des_right[$i];     ?></p>
          </li>


<?php

$i++;


}

?> 

</ul>

</div>


 </div> <!-- row -->
  </div>  <!-- container -->
</section><!-- /.features -->





<!-- .features mobile -->
<section class="features-mobile d-block d-sm-none">
  <div class="container">
    <article class="article-content">


      <!-- count the total no. of listings for mobile features -->

       <?php

        if( have_rows('mobile_app_features_mobile_view') ):

        while( have_rows('mobile_app_features_mobile_view') ): the_row();  ?>


      <h3><?php  echo get_sub_field("mobile_app_feature_head");  ?></h3>
      <h6><?php  echo get_sub_field("mobile_app_feature_desc");  ?> </h6>
    

    <?php

        endwhile;
        endif;

        ?>
      
    </article>
    <div class="features-mob-slider">
      <div id="FeaturesMobSlider" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">

          <?php
          $count_mobile_features =0;

          if( have_rows('mobile_app_features_mobile_view') ):

          while( have_rows('mobile_app_features_mobile_view') ): the_row();  


            if( have_rows('mobile_app_feature_listings') ):

          while( have_rows('mobile_app_feature_listings') ): the_row();  


        

        $count_mobile_features++;

        endwhile;
        endif;


        endwhile;
        endif;

        ?>


<!-- print the li elements for carousal according to the count_mobile_features -->
        <?php


        $mob_slider = 1;

        while($mob_slider<=$count_mobile_features){

          if($count_mobile_features == 1){  ?>

              <li data-target="#FeaturesMobSlider" data-slide-to="<?php  echo $mob_slider-1; ?>" class="active"></li>

              <?php


          }else{  ?>


              <li data-target="#FeaturesMobSlider" data-slide-to="<?php  echo $mob_slider-1; ?>"></li>

            <?php


              $mob_slider++;

          }

        }
        ?>



        
        </ol>
        <div class="carousel-inner">


          <?php
          $listing_mobile =1;

          if( have_rows('mobile_app_features_mobile_view') ):

          while( have_rows('mobile_app_features_mobile_view') ): the_row();  


            if( have_rows('mobile_app_feature_listings') ):

          while( have_rows('mobile_app_feature_listings') ): the_row();  


            if($listing_mobile ==1 ){ ?>


               <div class="carousel-item active">

              <?php


            }else{    ?>


                <div class="carousel-item">

            <?php

                }


            ?>
          
            <div class="features-slider-content">
              <figure><img src="<?php echo get_sub_field("mobile_app_feature_listing_image");?>"></figure>
              <h3><?php echo get_sub_field("mobile_app_feature_listings_heading");?></h3>
              <p><?php echo get_sub_field("mobile_app_feature_description");?></p>
              <a href="<?php echo get_sub_field("mobile_app_feature_listing_button_link");?>" class="btn btn-red"><?php echo get_sub_field("mobile_app_feature_listing_button_text");?></a>
            </div>
        </div>

    <?php

        $listing_mobile++;

        endwhile;
        endif;


        endwhile;
        endif;

        ?>

        </div>
      </div>
    </div>
  </div>
</section><!-- /.features mobile -->














<!-- .services -->
<section class="our-services">
  <div class="container">
    <!-- .section heading -->
    <div class="services-content section-head">

      <?php

     if( have_rows('service_section') ):

          while( have_rows('service_section') ): the_row(); ?>


            <h3><?php  echo get_sub_field("service_heading");    ?></h3>
      <p><?php  echo get_sub_field("service_description");    ?></p>

          <?php 


          endwhile;

        endif;

      ?>      
    </div><!-- /.section heading -->


    <!-- .services cards -->
    <div class="services-cards">
      <div class="row">


        <?php


        $args = array(
        'post_type' => 'services',
        'post_status' => 'publish',
        'posts_per_page' => -1
        );
  $services = get_posts($args);

 if (!empty($services)):
        foreach ($services as $service) :
          //set up global post data 'service'
        setup_postdata($service);

          /*global $post;

          $post  = $service;
*/

          $title = get_the_title($service);  //retrieve the title of post 'service'
          ?>
          <div class="col-xs-12 col-sm-4 col-md-4">
          <!-- .card -->
          <div class="card text-center">
            <figure class="icon d-none d-sm-block"><img src="<?php echo get_the_post_thumbnail_url($service); ?>" alt="<?php  echo $title;  ?>"></figure>
            <figure class="icon d-block d-sm-none"><img src="<?php echo get_the_post_thumbnail_url($service); ?>" alt="<?php  echo $title;  ?>"></figure>
            <h5><?php  echo $title;  ?></h5>
            <p><?php echo get_the_content($service); ?></p>
          </div><!-- /.card -->
        </div>

          <?php
        endforeach;
      endif;

    ?>
      </div>
    </div><!-- .services cards -->
  </div>
</section><!-- /.services -->




<!-- .our work -->
<section class="our-work">
  <div class="container">
    <!-- .section heading -->
    <div class="work-content section-head">
      <h3><?php the_field("our_work_heading");  ?></h3>
    </div><!-- /.section heading -->

    <div id="OurWorkSlider" class="carousel slide d-none d-sm-block" data-ride="carousel">
      <ol class="carousel-indicators">

        <?php

        

        $args = array(
        'post_type' => 'work',
        'post_status' => 'publish',
        'posts_per_page' => -1
        );
        $work = get_posts($args);


        //total no. of work posts

         $count_work = count($work);       

       
          $work_slider = $count_work/3;


          if(is_float($work_slider)){


           $work_slider =  (int)$work_slider +1 ;
       
            

          }else{


          $work_slider =$work_slider;


          }



          /*print the slider to total work slider times*/
          $current_slide  =1;

          while($current_slide<=$work_slider){

            if($current_slide==1){ ?>



<li data-target="#OurWorkSlider" data-slide-to="<?php echo $current_slide-1;  ?>" class="active"></li>

            <?php

            }else{ ?>

               <li data-target="#OurWorkSlider" data-slide-to="<?php echo $current_slide-1;  ?>"></li>

               <?php


            }

            $current_slide++;

          }

        ?>

        </ol>


      <div class="carousel-inner">

<div class="carousel-item active">
       <div class="row">

      <?php
        $args = array(
        'post_type' => 'work',
        'post_status' => 'publish',
        'posts_per_page' => 3
        );
      $work = get_posts($args);

      foreach ($work as $work) {        ?>
        <div class="col-xs-12 col-sm-4 col-md-4">
              <img src="<?php echo get_the_post_thumbnail_url($work);?>" class="d-block w-100" alt="item">
              <h4><?php  echo get_the_title($work); ?></h4>
        </div>

        <?php
      }

      ?>

</div>
</div> <!-- carousel-item active -->


<?php


      $args = array(
        'post_type' => 'work',
        'post_status' => 'publish',
        'posts_per_page' => -1
        );
  $work_posts = get_posts($args);

//get the left posts by removeing first three posts
  $left_work_posts = array_slice($work_posts, 3);

  $count_work = count($left_work_posts);

 $total_slides = $count_work/3;

  if(is_float($total_slides)){

    $total_slides = intval($total_slides) +1 ; 
    
  }




/*run the loop upto total slides*/

$slide_set = 1;

$initial_arr = $left_work_posts;

while ($slide_set <= $total_slides) {

   $length_arr = count($initial_arr);



  $array_sliced  = array_slice($initial_arr, 0,3);





  $initial_arr = array_slice($initial_arr, -($length_arr-3));





  ?>


          <div class="carousel-item">
          <div class="row">

            <?php

            
              foreach ($array_sliced as $work_post1) { ?>

              <div class="col-xs-12 col-sm-4 col-md-4">
                <img src="<?php echo get_the_post_thumbnail_url($work_post1);?>" class="d-block w-100" alt="item">
                <h4><?php  echo get_the_title($work_post1); ?></h4>
              </div>
                
<?php }
            
            ?>

          </div>
        </div>


<?php

  $slide_set++;


}



?>

      </div> <!-- carousel-inner -->
    </div> <!-- OurWorkSlider -->




 <div id="OurWorkSliderMobile" class="carousel slide d-block d-sm-none" data-ride="carousel">
      <ol class="carousel-indicators">


<?php


 $args = array(
        'post_type' => 'work',
        'post_status' => 'publish',
        'posts_per_page' => -1
        );
  $work_posts = get_posts($args);

  $count_work = 1;

  foreach ($work_posts as $work) { 


if($count_work ==1){ ?>

  <li data-target="#OurWorkSliderMobile" data-slide-to="<?php  echo $count_work-1; ?>" class="active"></li>

<?php
}else{

?>
  <li data-target="#OurWorkSliderMobile" data-slide-to="<?php  echo $count_work-1; ?>"></li>

<?php }
  

    $count_work++;

  }


?>
      </ol>


      <div class="carousel-inner">

<?php


 $args = array(
        'post_type' => 'work',
        'post_status' => 'publish',
        'posts_per_page' => -1
        );
  $work_posts = get_posts($args);

  $count_work = 1;

  foreach ($work_posts as $work) { 


if($count_work ==1){ ?>



        <div class="carousel-item active">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <img src="<?php echo get_the_post_thumbnail_url($work);?>" class="d-block w-100" alt="item">
              <h4><?php echo get_the_title($work);  ?></h4>
            </div>
          </div>
        </div>

        <?php
      }else{ ?>


 <div class="carousel-item">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <img src="<?php echo get_the_post_thumbnail_url($work); ?>" class="d-block w-100" alt="item">
              <h4><?php echo get_the_title($work);  ?></h4>
            </div>
          </div>
        </div>

<?php
      }

      $count_work++;

    }

        ?>

       

      </div>
    </div>
  </div>
</section><!-- /.our work -->












<!-- .our clients -->
<section class="our-clients">
  <div class="clients-content section-head">
    <h3><?php the_field("our_client_heading"); ?></h3>
  </div><!-- /.section heading -->
  <div class="container height-100 d-none d-sm-block">
    <!-- .our clients logos -->
    <div class="all-logos height-100">
      <div class="align-content">


        <?php

$args = array(
        'post_type' => 'clients',
        'post_status' => 'publish',
        'posts_per_page' => -1
        );
  $client_posts = get_posts($args);

  $total_clients = count($client_posts);

  $client_rows = $total_clients/5;


  if(is_float($client_rows)){

    $client_rows = intval($client_rows) + 1; 
  }


$row  =1;

$initial_arr = $client_posts;

while ($row <= $client_rows) { 

    $length_arr = count($initial_arr);



  $array_sliced  = array_slice($initial_arr, 0,5);



  $initial_arr = array_slice($initial_arr, -($length_arr-5));



  ?>

   <div class="custom-row">

    <?php

    $array_slice  = array_slice($client_posts, 0,5);


    foreach ($array_sliced as $client_posts) {  ?>
      

          <div class="custom-col-2">
            <figure class="client-logo"><img src="<?php echo get_the_post_thumbnail_url($client_posts);?>" alt="CAT"></figure>
          </div>

<?php


    }


    ?>


    <?php

    $row++;

    ?>

  </div>

  <?php
  
}

        ?>

      
      </div>
    </div><!-- .our clients logos -->
  </div>




      </div>
    </div><!-- .our clients logos -->
  </div>
  <div class="container header-wrapper d-block d-sm-none">
    <!-- .our clients logos mobile -->
    <div class="all-logos">
      <div class="align-content">

          <?php

$args = array(
        'post_type' => 'clients',
        'post_status' => 'publish',
        'posts_per_page' => -1
        );
  $client_posts = get_posts($args);

  $total_clients = count($client_posts);

  $client_rows = $total_clients/4;


  if(is_float($client_rows)){

    $client_rows = intval($client_rows) + 1; 
  }


$row  =1;

$initial_arr = $client_posts;

while ($row <= $client_rows) { 

    $length_arr = count($initial_arr);



  $array_sliced  = array_slice($initial_arr, 0,4);



  $initial_arr = array_slice($initial_arr, -($length_arr-4));



  ?>

   <div class="custom-row">

    <?php

    $array_slice  = array_slice($client_posts, 0,5);


    foreach ($array_sliced as $client_posts) {  ?>
      

          <div class="custom-col-2">
            <figure class="client-logo"><img src="<?php echo get_the_post_thumbnail_url($client_posts);?>" alt="CAT"></figure>
          </div>

<?php


    }


    ?>


    <?php

    $row++;
  
}

        ?>

      
      </div>
    </div><!-- .our clients logos -->
  </div>

       
      </div>
    </div><!-- .our clients logos mobile -->
  </div>
</section><!-- /.our clients -->




<?php get_footer();?>