<?php


//includes lib files

$theme_includes = [
'lib/acf_customization.php',
'lib/custom_post_types.php',
'lib/theme_support.php'
];


foreach ($theme_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);




?>