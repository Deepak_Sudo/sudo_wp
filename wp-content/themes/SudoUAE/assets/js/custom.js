// =========== Custom Javascript =========== //

// scroll to top button
$(document).ready(function() {
	$(window).scroll(function() {
		if ($(this).scrollTop() > 25) {
			$('#toTopBtn').fadeIn();
		} else {
			$('#toTopBtn').fadeOut();
		}
	});
	$('#toTopBtn').click(function() {
		$("html, body").animate({
			scrollTop: 0
		}, 1000);
		return false;
	});
});

// humburger menu
$('#toggle').click(function() {
	$(this).toggleClass('active');
	$('#overlay').toggleClass('open');
});

// tabs
$(function() {
  var $tabButtonItem = $('#tab-button li'),
  $tabSelect = $('#tab-select'),
  $tabContents = $('.tab-contents'),
  activeClass = 'is-active';

  $tabButtonItem.first().addClass(activeClass);
  $tabContents.not(':first').hide();

  $tabButtonItem.find('a').on('click', function(e) {
    var target = $(this).attr('href');

    $tabButtonItem.removeClass(activeClass);
    $(this).parent().addClass(activeClass);
    $tabSelect.val(target);
    $tabContents.hide();
    $(target).fadeIn();
    e.preventDefault();
  });

  $tabSelect.on('change', function() {
    var target = $(this).val(),
    targetSelectNum = $(this).prop('selectedIndex');

    $tabButtonItem.removeClass(activeClass);
    $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
    $tabContents.hide();
    $(target).fadeIn();
  });
});