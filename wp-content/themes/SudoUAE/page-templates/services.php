<?php /* Template Name:Services */ 
get_header();


?>

<!-- .marketing agency -->
<section class="marketing-agency">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <article class="article-descp">

          <?php


        if( have_rows('service_banner') ):

        while( have_rows('service_banner') ): the_row();

      

          ?>
          <h3><?php echo get_sub_field('service_banner_main_head');    ?></h3>
          <h4><?php echo get_sub_field('service_banner_sub_heading');    ?></h4>
          <p><?php echo get_sub_field('service_banner_desciption');    ?></p>
          <a href="<?php echo get_sub_field('service_banner_button_link');    ?>" class="btn btn-red width-160"><?php echo get_sub_field('service_banner_button_text');    ?></a>
         
        </article>
      </div>
      <div class="col-6">
        <figure>
          <img src="<?php echo get_sub_field('service_banner_image');    ?>" alt="Digital Marketing Agency">
        </figure>
      </div>
    </div>
  </div>
</section><!-- /.marketing agency -->

 <?php

  endwhile;
  endif;
?>
<!-- .offerings -->
<section class="offerings">
  <div class="container">
    <article class="article-descp">

       <?php


        if( have_rows('service_offering') ):

        while( have_rows('service_offering') ): the_row();

      ?>


      <h3><?php echo get_sub_field('heading');?></h3>
      <p><?php echo get_sub_field('description');?></p>
    </article>
    <div class="offerings-card">
      <div class="row">
        <?php

           if( have_rows('offers_listing') ):

        while( have_rows('offers_listing') ): the_row();



        ?>

        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <h4><?php echo get_sub_field('offer_heading');   ?>sf</h4>
            <p><?php echo get_sub_field('offering_description');   ?>sf</p>
          </div>
        </div>

        <?php

          endwhile;
        endif;

        ?>


      </div>
    </div>
  </div>

  <?php

endwhile;
endif;

  ?>
</section><!-- /.offerings -->  
<!-- .talk to experts -->
<section class="talk-to-experts">
  <div class="container">
    <div class="form-head text-center">
      <h3>Talk to our experts and avail free seo audit repot</h3>
      <p>We at SUDO, are fully equipped to help you realise your digital goal. Kickstart your campaigns and maximise your market reach with us. Your success is our success.</p>
    </div>
    <form class="get-report">
      <ul class="fieldset">
        <li class="field">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Your Website URL" required="">
          </div>
        </li>
        <li class="field">
          <div class="form-group">
            <input type="email" class="form-control" placeholder="Email" required="">
          </div>
        </li>
        <li class="button">
          <button type="submit" class="btn btn-white">GET REPORT</button>
        </li>
      </ul>
    </form>
  </div>
</section><!-- /.talk to experts -->
<!-- .business growth -->
<section class="business-growth">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <article class="article-descp">
          <h3>Maximize your business growth</h3>
          <h4>Crafting out a Niche for your online Business</h4>
          <p>We at SUDO, are a professional, affordable and reliable online marketing agency. With our end-to-end digital marketing services, we help you reach out to a larger customer base and drive more traffic to your website. Our team of experts work with you to optimise your website, to augment your visibility, cultivate brand awareness and improve your searchengine ranking. We redefine digital marketing by using fresh and relevant data, advancedtechnology and media in innovative ways. We help you understand how your audience will discover you.</p>
          <a href="#" class="btn btn-red width-160">Start now</a>
        </article>
      </div>
      <div class="col-6">
        <figure class="text-center">
          <img src="<?php echo get_template_directory_uri().'/assets/img/business-growth.png'; ?>" alt="Business Growth">
        </figure>
      </div>
    </div>
  </div>
</section><!-- /.business growth -->
<!-- .growth cards -->
<section class="growth-cards">
  <div class="container">
    <div class="offerings-card">
      <div class="row">
        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <h4>Developing Strategies</h4>
            <p>At SUDO, we grow business by building a strong online presence using data-driven strategies and best practices to deliver quality traffic to your website.</p>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <h4>Driving Profitable Traffic</h4>
            <p>At SUDO, we grow business by building a strong online presence using data-driven strategies and best practices to deliver quality traffic to your website.</p>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <h4>Increase Customer Retention & Conversations</h4>
            <p>At SUDO, we grow business by building a strong online presence using data-driven strategies and best practices to deliver quality traffic to your website.</p>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <h4>Online Media Management</h4>
            <p>At SUDO, we grow business by building a strong online presence using data-driven strategies and best practices to deliver quality traffic to your website.</p>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <h4>Website Audit</h4>
            <p>At SUDO, we grow business by building a strong online presence using data-driven strategies and best practices to deliver quality traffic to your website.</p>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <h4>Penalty Recovery</h4>
            <p>At SUDO, we grow business by building a strong online presence using data-driven strategies and best practices to deliver quality traffic to your website.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- /.growth cards -->
<!-- .social marketing -->
<section class="social-marketing">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <article class="article-descp">
          <h3>Social Media marketing</h3>
          <h4>Helping your brand get noticed and increase your online presence.</h4>
          <p>We at SUDO, are a professional, affordable and reliable online marketing agency. With our end-to-end digital marketing services, we help you reach out to a larger customer base and drive more traffic to your website. Our team of experts work with you to optimise your website, to augment your visibility, cultivate brand awareness and improve your searchengine ranking. We redefine digital marketing by using fresh and relevant data, advancedtechnology and media in innovative ways. We help you understand how your audience will discover you.</p>
          <a href="#" class="btn btn-red width-160">Start now</a>
        </article>
      </div>
      <div class="col-6">
        <figure class="text-center">
          <img src="<?php echo get_template_directory_uri().'/assets/img/media-marketing.png'; ?>" alt="Social Media marketing">
        </figure>
      </div>
    </div>
  </div>
</section><!-- /.social marketing -->
<!-- .why choose us -->
<section class="why-choose-us">
  <div class="container">
    <article class="article-descp">
      <h3>why choose us</h3>
      <h4>A choice that makes the difference</h4>
      <p>We at SUDO, are a professional, affordable and reliable online marketing agency. With our end-to-end digital marketing services, we help you reach out to a larger customer base and drive more traffic to your website.</p>
    </article>
    <div class="cards-group">
      <div class="row">
        <div class="col-4">
          <div class="card">
            <h5>Skills and Expertise</h5>
            <div class="box-content">
              <p>Experience matters more than anything else. We have the resources and the relevant expertise that is required to deliver on small, medium and large projects successfully.</p>
            </div>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <h5>100% Guarantee</h5>
            <div class="box-content">
              <p>We are a team of passionate and goal-oriented people, confident of delivering 100% guaranteed first page results within a few months of using our services.</p>
            </div>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <h5>Increased Revenue</h5>
            <div class="box-content">
              <p>No matter how well your website ranks on SERPs, the ultimate goal is to boost sales and generate revenue. At SUDO, we effectively optimise your website for increased sales.</p>
            </div>
          </div>
        </div>
        <div class="col-2"></div>
        <div class="col-4">
          <div class="card">
            <h5>Constantly Evolving</h5>
            <div class="box-content">
              <p>At SUDO, we are constantly evolving by researching new and upcoming technologies to enable our customers in finding their business solutions more conveniently and efficiently.</p>
            </div>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <h5>Online Reporting /Analysis</h5>
            <div class="box-content">
              <p>We constantly monitor and analyse your website performance and make improvements as and when there is a need. We send online report and analysis for your review.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- /.why choose us -->


<?php  get_footer();         ?>