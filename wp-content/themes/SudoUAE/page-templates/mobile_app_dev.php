<?php /* Template Name: Mobile Application Development */ 

get_header();

?>

<!-- .hero -->
<section class="hero banner-full dual-section-banner">
  <div class="container height-100">
    <div class="row height-100">
      <div class="col-6 height-100">
        <figure class="text-center">
          <img src="<?php echo get_template_directory_uri().'/assets/img/thinking-forward.png' ?>" alt="Always Thinking Forward">
        </figure>
      </div>
      <div class="col-6 height-100">
        <article class="article">
          <h1>Always Thinking Forward</h1>
          <p>We prioritize sustainabiity and scalability when developing our custom mobile apps so your business's operations won't be disrupted by system updates and service disruptions. We create native and cross-platform mobile apps for iOS and Android that leverage all potential device and operating system features.</p>
        </article>
      </div>
    </div>
  </div>
</section><!-- /.hero -->
<!-- .why choose us -->
<section class="why-choose-sudo banner-full opacity-7">
  <div class="container relative-pos height-100">
    <div class="section-head section-head-dev">
      <h3>Why Sudo For Mobile <br> application development?</h3>
      <p>we aspire to inspire</p>
    </div>
    <!-- .cards -->
    <div class="why-choose-us-cards">
      <div class="row">
        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <div class="card-content">
              <h4>IDENTIFYING THE BUSINESS OBJECTIVES</h4>
              <p>We at SUDO work closely with our clients to identify their business objectives and with them build mobile applications that have a positive impact on the life of people. By exploring the best possible mobile app solutions, we aim to fulfill the customer requirement. Let us know your business goal and we will help you shape your dream mobile applications.</p>
            </div>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <div class="card-content">
              <h4>DISCOVER THE PERFECT BLEND OF TAILORED SERVICES</h4>
              <p>With the harmonious blend of design, development, marketing skills, we at SUDO promise you, we will use knowledge and expertise to help you succeed by enhancing your online presence in the jam-packed mobile app industry. We transport fluidity in your business by discovering compelling iOS, Android, and Cross-platform mobile apps, tailored to business requirement.</p>
            </div>
          </div>
        </div>
        <div class="col-4">
          <div class="card">
            <span class="icon"></span>
            <div class="card-content">
              <h4>HELPING YOU BUILD THE NEXT BIG IDEA</h4>
              <p>At SUDO, we are passionate about delivering high-quality iOS, Android, and Cross-platform mobile applications, aiming to fulfill the futuristic business requirement. We set trends and elevate your brand value by helping you build solutions that can transform your entire business perspective. We have paved the path of success for many brands and businesses by building trusted mobile applications.</p>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.cards -->
  </div>
</section><!-- /.why choose us -->
<!-- .platforms -->
<section class="platforms">
  <div class="container">
    <div class="section-head section-head-dev width-full">
      <h3>Solutions, applications and platforms</h3>
      <p>we aspire to inspire</p>
    </div>
    <!-- .cards -->
    <div class="platform-cards">
      <div class="row">
        <div class="col-3">
          <div class="card">
            <span class="icon text-center">
              <img src="<?php echo get_template_directory_uri().'/assets/img/ios-dev.png'; ?>" alt="IoS app development">
            </span>
            <h4>IoS app development</h4>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business</p>
          </div>
        </div>
        <div class="col-3">
          <div class="card active">
            <span class="icon text-center">
              <img src="<?php echo get_template_directory_uri().'/assets/img/android-dev.png';?>" alt="Android app development">
            </span>
            <h4>Android app development</h4>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business</p>
          </div>
        </div>
        <div class="col-3">
          <div class="card">
            <span class="icon text-center">
              <img src="<?php echo get_template_directory_uri().'/assets/img/cross-platform-dev.png' ?>" alt="cross-platform app development">
            </span>
            <h4>cross-platform app development</h4>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business</p>
          </div>
        </div>
        <div class="col-3">
          <div class="card">
            <span class="icon text-center">
              <img src="<?php echo get_template_directory_uri().'/assets/img/game-dev.png' ?>" alt="2d/3d game development">
            </span>
            <h4>2d/3d game development</h4>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business</p>
          </div>
        </div>
      </div>
    </div><!-- /.cards -->
    <!-- .step process -->
    <div class="step-process">
      <div class="step-head text-center">
        <h4>Fostering innovation, delivering what you want</h4>
        <h5>A 7-step process to get started with your mobile app</h5>
      </div>
      <!-- .step process tabs -->
      <div class="tabs">
        <div class="tab-button-outer">
          <ul id="tab-button">
            <li><a href="#tab01">Concept building</a></li>
            <li><a href="#tab02">Wireframing</a></li>
            <li><a href="#tab03">Apis, SERVER, AND BACKEND</a></li>
            <li><a href="#tab04">CREATING PROTOTYPES</a></li>
            <li><a href="#tab05">DEVELOPMENT</a></li>
            <li><a href="#tab06">TESTING</a></li>
            <li><a href="#tab07">Deployment</a></li>
          </ul>
        </div>
        <!-- .options for smaller device -->
        <div class="tab-select-outer">
          <select id="tab-select">
            <option value="#tab01">Concept building</option>
            <option value="#tab02">Wireframing</option>
            <option value="#tab03">Apis, SERVER, AND BACKEND</option>
            <option value="#tab04">CREATING PROTOTYPES</option>
            <option value="#tab05">DEVELOPMENT</option>
            <option value="#tab06">TESTING</option>
            <option value="#tab07">Deployment</option>
          </select>
        </div><!-- /.options for smaller device -->
        <div id="tab01" class="tab-contents opacity-7">
          <article class="article-descp">
            <h3>Concept building</h3>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business/ development requirement analysis, etc. to reach an idea that can meet the expectation of the target audience. In this stage, almost everything is analysed from the budget, timeline, hardware/software requirement and other strategic requirements.</p>
            <a href="#" class="btn btn-red width-160">get a free quote</a>
          </article>
        </div>
        <div id="tab02" class="tab-contents opacity-7">
          <article class="article-descp">
            <h3>Wireframing</h3>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business/ development requirement analysis, etc. to reach an idea that can meet the expectation of the target audience. In this stage, almost everything is analysed from the budget, timeline, hardware/software requirement and other strategic requirements.</p>
            <a href="#" class="btn btn-red width-160">get a free quote</a>
          </article>
        </div>
        <div id="tab03" class="tab-contents opacity-7">
          <article class="article-descp">
            <h3>Apis, SERVER, AND BACKEND</h3>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business/ development requirement analysis, etc. to reach an idea that can meet the expectation of the target audience. In this stage, almost everything is analysed from the budget, timeline, hardware/software requirement and other strategic requirements.</p>
            <a href="#" class="btn btn-red width-160">get a free quote</a>
          </article>
        </div>
        <div id="tab04" class="tab-contents opacity-7">
          <article class="article-descp">
            <h3>CREATING PROTOTYPES</h3>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business/ development requirement analysis, etc. to reach an idea that can meet the expectation of the target audience. In this stage, almost everything is analysed from the budget, timeline, hardware/software requirement and other strategic requirements.</p>
            <a href="#" class="btn btn-red width-160">get a free quote</a>
          </article>
        </div>
        <div id="tab05" class="tab-contents opacity-7">
          <article class="article-descp">
            <h3>DEVELOPMENT</h3>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business/ development requirement analysis, etc. to reach an idea that can meet the expectation of the target audience. In this stage, almost everything is analysed from the budget, timeline, hardware/software requirement and other strategic requirements.</p>
            <a href="#" class="btn btn-red width-160">get a free quote</a>
          </article>
        </div>
        <div id="tab06" class="tab-contents opacity-7">
          <article class="article-descp">
            <h3>TESTING</h3>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business/ development requirement analysis, etc. to reach an idea that can meet the expectation of the target audience. In this stage, almost everything is analysed from the budget, timeline, hardware/software requirement and other strategic requirements.</p>
            <a href="#" class="btn btn-red width-160">get a free quote</a>
          </article>
        </div>
        <div id="tab07" class="tab-contents opacity-7">
          <article class="article-descp">
            <h3>Deployment</h3>
            <p>Without a perfect strategy in place, the development of any application is meaningless. Planning helps in identifying the weaknesses and strengths of a company, thereafter the development can kick-off efficiently. It starts with market research, competitor analysis, business/ development requirement analysis, etc. to reach an idea that can meet the expectation of the target audience. In this stage, almost everything is analysed from the budget, timeline, hardware/software requirement and other strategic requirements.</p>
            <a href="#" class="btn btn-red width-160">get a free quote</a>
          </article>
        </div>
      </div><!-- /.step process tabs -->
    </div><!-- /.step process -->
  </div>
</section><!-- /.platforms -->
<!-- .housecall -->
<section class="housecall">
  <div class="container height-100">
    <div id="HousecallSlider" class="carousel slide height-100" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#HousecallSlider" data-slide-to="0" class="active"></li>
        <li data-target="#HousecallSlider" data-slide-to="1"></li>
        <li data-target="#HousecallSlider" data-slide-to="2"></li>
        <li data-target="#HousecallSlider" data-slide-to="3"></li>
      </ol>
      <div class="carousel-inner height-100">
        <div class="carousel-item height-100 active">
          <div class="housecall-slide">
            <div class="content-align">
              <div class="row">
                <div class="col-6">
                  <figure class="text-right"><img src="<?php echo get_template_directory_uri().'/assets/img/thinking-forward.png' ?>" alt="item"></figure>
                </div>
                <div class="col-6">
                  <article class="article">
                    <h3>housecall</h3>
                    <p>Quick response Mobile App appropriate for medical emergencies at home. Housecall is an iOS- and Android-based Mobile Application bringing 'doctors on call' to your location.</p>
                    <a href="#" class="btn-store"><img src="<?php echo get_template_directory_uri().'/assets/img/app-store-logo.png '; ?>"></a>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="carousel-item height-100">
          <div class="housecall-slide">
            <div class="content-align">
              <div class="row">
                <div class="col-6">
                  <figure class="text-right"><img src="<?php echo get_template_directory_uri().'/assets/img/thinking-forward.png' ;?>" alt="item"></figure>
                </div>
                <div class="col-6">
                  <article class="article">
                    <h3>housecall</h3>
                    <p>Quick response Mobile App appropriate for medical emergencies at home. Housecall is an iOS- and Android-based Mobile Application bringing 'doctors on call' to your location.</p>
                    <a href="#" class="btn-store"><img src="<?php echo get_template_directory_uri().'/assets/img/app-store-logo.png' ?>"></a>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="carousel-item height-100">
          <div class="housecall-slide">
            <div class="content-align">
              <div class="row">
                <div class="col-6">
                  <figure class="text-right"><img src="<?php echo get_template_directory_uri().'/assets/img/thinking-forward.png' ?>" alt="item"></figure>
                </div>
                <div class="col-6">
                  <article class="article">
                    <h3>housecall</h3>
                    <p>Quick response Mobile App appropriate for medical emergencies at home. Housecall is an iOS- and Android-based Mobile Application bringing 'doctors on call' to your location.</p>
                    <a href="#" class="btn-store"><img src="<?php echo get_template_directory_uri().'/assets/img/app-store-logo.png' ?>"></a>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="carousel-item height-100">
          <div class="housecall-slide">
            <div class="content-align">
              <div class="row">
                <div class="col-6">
                  <figure class="text-right"><img src="<?php echo get_template_directory_uri().'/assets/img/thinking-forward.png' ?>" alt="item"></figure>
                </div>
                <div class="col-6">
                  <article class="article">
                    <h3>housecall</h3>
                    <p>Quick response Mobile App appropriate for medical emergencies at home. Housecall is an iOS- and Android-based Mobile Application bringing 'doctors on call' to your location.</p>
                    <a href="#" class="btn-store"><img src="<?php echo get_template_directory_uri().'/assets/img/app-store-logo.png' ?>"></a>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- .housecall -->
<!-- .gurantee -->
<section class="gurantee">
  <div class="container">
    <div class="section-head section-head-dev">
      <h3>our commitment & gurantee</h3>
      <p>Sudo delivers 100% scalable, robust and high-performance software's, mobile apps and web applications.</p>
    </div>
    <!-- .gurantee cards -->
      <ul class="gurantee-cards">
        <li class="card">
          <span class="icon"></span>
          <h4>100% TRANSPARENCY</h4>
          <p>Take transparency to the next level. We at SUDO practice 100% transparency during the entire development process by keeping our clients fully aware of the progress we make towards their goals.</p>
        </li>
        <li class="card">
          <span class="icon"></span>
          <h4>ON-TIME DELIVERY</h4>
          <p>Take transparency to the next level. We at SUDO practice 100% transparency during the entire development process by keeping our clients fully aware of the progress we make towards their goals.</p>
        </li>
        <li class="card">
          <span class="icon"></span>
          <h4>FREE 30 DAYS SUPPORT</h4>
          <p>Take transparency to the next level. We at SUDO practice 100% transparency during the entire development process by keeping our clients fully aware of the progress we make towards their goals.</p>
        </li>
        <li class="card">
          <span class="icon"></span>
          <h4>FLEXIBLE ENGAGEMENTS</h4>
          <p>Take transparency to the next level. We at SUDO practice 100% transparency during the entire development process by keeping our clients fully aware of the progress we make towards their goals.</p>
        </li>
        <li class="card">
          <span class="icon"></span>
          <h4>24X7 SUPPORT</h4>
          <p>Take transparency to the next level. We at SUDO practice 100% transparency during the entire development process by keeping our clients fully aware of the progress we make towards their goals.</p>
        </li>
      </ul>
    <!-- /.gurantee cards -->
  </div>
</section><!-- /.gurantee -->
<!-- .contact us -->
<section class="contact-us banner-full opacity-7 opacity-8">
  <div class="container">
    <div class="contact-us-form content-align">
      <h2>Let's Begin Writing</h2>
      <form class="custom-form">
        <div class="row">
          <div class="col-6 space-right">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="First Name*" required="">
            </div>
          </div>
          <div class="col-6 space-left">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Last Name*" required="">
            </div>
          </div>
        </div>
        <div class="form-group">
          <input type="email" class="form-control" placeholder="Email Address*" required="">
        </div>
        <div class="row">
          <div class="col-6 space-right">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Company / Organization">
            </div>
          </div>
          <div class="col-6 space-left">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="$0 to $10,000">
            </div>
          </div>
        </div>
        <div class="form-group">
          <textarea class="form-control height-auto" rows="4" placeholder="Message"></textarea>
        </div>
        <button type="submit" class="btn btn-red">Get A Free Quote</button>
      </form>
    </div>
  </div>
</section><!-- /.contact us -->
<!-- .get in touch -->
<section class="get-in-touch">
  <div class="container">
    <h2>Get In Touch</h2>
    <ul class="social-nav">
      <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
      <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
    </ul>
    <div class="btn-group">
      <a href="#" class="btn btn-white">E-mail</a>
      <a href="#" class="btn btn-white">Call</a>
    </div>
  </div>
</section><!-- /.get in touch -->


































<?php  get_footer();         ?>