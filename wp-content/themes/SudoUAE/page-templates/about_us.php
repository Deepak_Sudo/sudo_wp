<?php /* Template Name: About Us*/ 

get_header();

?>

<!-- .hero -->
<section class="hero banner-full opacity-7">
  <div class="container">
    <div class="hero-content">
      <h1><?php the_field('about_banner_head');?></h1>
      <p><?php the_field('about_banner_content');?></p>
    </div>
  </div>
</section><!-- /.hero -->
<!-- .process -->
<section class="process">
  <div class="container">
    
  </div>
</section><!-- /.process -->
<!-- .services -->
<section class="services banner-full">
  <div class="container">
    <div class="serices-content content-align">
      <p><?php the_field("4cs_heading"); ?></p>
      <h2><?php the_field("4cs_description"); ?></h2>
    </div>
  </div>
</section><!-- /.services -->
<!-- .contact us -->
<section class="contact-us banner-full opacity-7 opacity-8">
  <div class="container">
    <div class="contact-us-form content-align">
      <h2><?php the_field('contact_form_heading');?></h2>



      <form class="custom-form">
        <div class="row">
          <div class="col-6 space-right">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="First Name*" required="">
            </div>
          </div>
          <div class="col-6 space-left">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Last Name*" required="">
            </div>
          </div>
        </div>
        <div class="form-group">
          <input type="email" class="form-control" placeholder="Email Address*" required="">
        </div>
        <div class="row">
          <div class="col-6 space-right">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Company / Organization">
            </div>
          </div>
          <div class="col-6 space-left">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="$0 to $10,000">
            </div>
          </div>
        </div>
        <div class="form-group">
          <textarea class="form-control height-auto" rows="4" placeholder="Message"></textarea>
        </div>
        <button type="submit" class="btn btn-red">Get A Free Quote</button>
      </form>

      
    </div>
  </div>
</section><!-- /.contact us -->




<?php  get_footer();         ?>