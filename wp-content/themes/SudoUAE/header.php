<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet"  href="<?php echo get_template_directory_uri().'/assets/bootstrap/css/bootstrap.min.css'; ?>">
<!-- Icons CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css" >
<!-- Custom CSS -->
<link rel="stylesheet"  href="<?php echo get_template_directory_uri().'/assets/css/custom.css'; ?>">
<title>Sudo UAE</title>
</head>
<body>
<!-- .header -->
<header class="header">
  <div class="container header-wrapper">
    <a href="#" class="brand-logo">
      <img  src="<?php echo get_template_directory_uri().'/assets/img/brand-logo.png'; ?>" alt="Sudo">
    </a>
  </div>
</header><!-- .header -->