<?php

// Our custom post type function
function create_posttype() {
 
    register_post_type( 'services',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Services' ),
                'singular_name' => __( 'services' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'services'),
            'show_in_rest' => true,
            'supports' => array( 'title','thumbnail','editor'),
         
        )
    );



register_post_type( 'clients',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Clients' ),
                'singular_name' => __( 'clients' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'clients'),
            'show_in_rest' => true,
            'supports' => array( 'title','thumbnail','editor'),    
 
        )
    );


register_post_type( 'work',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Work' ),
                'singular_name' => __( 'work' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'work'),
            'show_in_rest' => true,
            'supports' => array( 'title','thumbnail','editor'),
           
 
        )
    );






}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );


?>
