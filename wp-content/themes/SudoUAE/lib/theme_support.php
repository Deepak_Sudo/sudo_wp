<?php

/*
 * Enable support Post Thumbnails
 * See https://codex.wordpress.org/Post_Thumbnails
 */
add_theme_support( 'post-thumbnails' );


/*enable theme menu */
add_theme_support('menus');


//register two different navs
function register_my_menu() {
	  register_nav_menus( array(  
	'our_services' => __( 'Our Services', 'SudoUAE' ),  
	'our_links' => __('Our Links', 'SudoUAE')  
	) ); 
}
add_action( 'init', 'register_my_menu' );


?>